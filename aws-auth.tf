data "aws_availability_zones" "available" {
}

provider "kubernetes" {
  host                   = var.cluster_endpoint
  cluster_ca_certificate = base64decode(var.cluster_certificate_authority_data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_id]
    command     = "aws"
  }
}

locals {
  kubeconfig = yamlencode(
    {
      apiVersion      = "v1"
      kind            = "Config"
      current-context = "terraform"
      clusters = [{
        name = var.cluster_id
        cluster = {
          certificate-authority-data = var.cluster_certificate_authority_data
          server                     = var.cluster_endpoint
        }
      }]
      contexts = [{
        name = "terraform"
        context = {
          cluster = var.cluster_id
          user    = "terraform"
        }
      }]
      users = [{
        name = "terraform"
        user = {
          token = var.token
        }
      }]
    }
  )
  current_auth_configmap = yamldecode(var.aws_auth_configmap_yaml)

  merged_permissions = {
    data = {
      mapRoles = replace(yamlencode(
        distinct(concat(
          yamldecode(local.current_auth_configmap.data.mapRoles), var.map_roles)
      )), "\"", "")
      mapUsers    = yamlencode(var.map_users)
      mapAccounts = yamlencode(var.map_accounts)
    }
  }
}

resource "null_resource" "patch_aws_auth_configmap" {
  triggers = {
    #cmd_patch = <<EOT
    #  kubectl patch configmap/aws-auth -n kube-system --type merge -p '${chomp(jsonencode(local.merged_permissions))}' --kubeconfig /tmp/config <(echo $KUBECONFIG | base64 --decode)
    #EOT
    cmd_patch = <<EOT
        export KUBECONFIG=/tmp/config
        aws eks update-kubeconfig --name ${var.cluster_id} --region ${var.region}
        kubectl patch configmap/aws-auth -n kube-system --type merge -p '${chomp(jsonencode(local.merged_permissions))}' --kubeconfig /tmp/config
    EOT
  }
  #kubectl patch configmap/aws-auth -n kube-system --type merge -p '${chomp(jsonencode(local.merged_permissions))}' --kubeconfig /tmp/config <(echo $KUBECONFIG | base64 --decode)

  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command     = self.triggers.cmd_patch

    #environment = {
    #  KUBECONFIG = base64encode(local.kubeconfig)
    #}
  }
}
