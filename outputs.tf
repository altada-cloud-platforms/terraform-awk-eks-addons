output "aws_auth_configmap_yaml" {
  description = "Formatted yaml output for aws-auth configmap."
  value       = local.merged_permissions
}

output "kubeconf" {
  description = "base64 encoded kubeconfig"
  value       = base64encode(local.kubeconfig)
}
